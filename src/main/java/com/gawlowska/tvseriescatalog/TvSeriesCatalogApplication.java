package com.gawlowska.tvseriescatalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvSeriesCatalogApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvSeriesCatalogApplication.class, args);
	}

}
